/**
 * 
 */
package com.colt.novitas.test.db;

import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.apache.ibatis.jdbc.ScriptRunner;

/**
 * Singleton test db updater service, this is a handy service when testing API clients which
 * do not use Spring
 * 
 * @author omerio
 *
 */
public class TestDataService {
    
    private static final TestDataService INSTANCE = new TestDataService();
    
    private static final String DB_CONFIG_FILE = "/test.properties";
    private static final String TEST_SQL_FILE = "/test_data.sql";
    
    private static final String DRIVER_KEY = "jdbc.driverClassName";
    private static final String URL_KEY = "jdbc.url";
    private static final String USER_KEY = "jdbc.username";
    private static final String PASS_KEY = "jdbc.password";
    
    private String dbDriver;
    private String dbConnectionUrl;
    private String dbUser;
    private String dbPassword;
    
    private Properties props;
    
    private TestDataService() {
        super();  
    }
    
    /**
     * Get the singleton service
     * @return
     */
    public static TestDataService getInstance() {
        return INSTANCE;
    }
    
    /**
     * Run a SQL query, the test will fail if the method fails. Only update queries are supported
     * insert, delete, etc...
     * @param query
     */
    /*public void runSQLUpdateQuery(String query) {
        Connection dbConnection = null;

        try {

            dbConnection = getDBConnection();
            
            dbConnection.setAutoCommit(true);
            
            Statement statement = dbConnection.createStatement();
            
            statement.executeUpdate(query);
            
        } catch(Exception e) {
            e.printStackTrace();

            fail("Failed to create test data, error: " + e.getMessage());
        
        } finally {
            this.closeConnection(dbConnection);
        }
    }

    *//**
     * Run the test_data.sql file to update the database to a known state 
     * before the test. The test will fail if this method fails
     *//*
    public void createTestData() {
        
        System.out.println("Creating test data");
        
        Connection dbConnection = null;

        try {

            dbConnection = getDBConnection();
            // Initialize object for ScripRunner
            ScriptRunner scriptRunner = new ScriptRunner(dbConnection);
            scriptRunner.setStopOnError(true);

            // Give the input file to Reader
            Reader reader = new BufferedReader(new InputStreamReader(
                    TestDataService.class.getResourceAsStream(TEST_SQL_FILE), "UTF-8"));

            // Exctute script
            scriptRunner.runScript(reader);
            
            System.out.println("Test data created successfully");

        } catch(Exception e) {
            e.printStackTrace();

            fail("Failed to create test data, error: " + e.getMessage());
        
        } finally {
            this.closeConnection(dbConnection);
        }

    }
    
    *//**
     * Get the value for a property defined in test.properties
     * @param key
     * @return
     *//*
    public String getTestProperty(String key) {
        return this.props.getProperty(key);
    }
    
    private void closeConnection(Connection dbConnection) {
        try {
            if(dbConnection != null) {
                dbConnection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            fail("Failed to close db connection, error: " + e.getMessage());
        }
    }

    private Connection getDBConnection() throws ClassNotFoundException, SQLException, IOException {

        this.loadDbProperties();

        Class.forName(dbDriver);

        Connection dbConnection = DriverManager.getConnection(dbConnectionUrl, dbUser,dbPassword);

        return dbConnection;

    }
    
    private void loadDbProperties() throws IOException {
        if(props == null) {
            
            InputStream stream = TestDataService.class.getResourceAsStream(DB_CONFIG_FILE);
            props = new Properties();
            props.load(stream);
            
            this.dbConnectionUrl = props.getProperty(URL_KEY);
            this.dbDriver = props.getProperty(DRIVER_KEY);
            this.dbUser = props.getProperty(USER_KEY);
            this.dbPassword = props.getProperty(PASS_KEY);
            
        }
    }*/

}
