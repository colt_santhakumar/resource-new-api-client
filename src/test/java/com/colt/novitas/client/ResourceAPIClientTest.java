package com.colt.novitas.client;

import org.junit.experimental.categories.Category;

import com.colt.novitas.test.category.UnitTest;

@Category(UnitTest.class)
public class ResourceAPIClientTest {
      
    private static final String ENV = "DEV";
    private static final String KEY = "callback.api.rest.url";
    private static final String VALUE = "http://amsdcp13:8330/callback-api/rest/";
    
    private static final String KEY2 = "siebel.assets.url";
    private static final String VALUE2 = "http://WBMEAIIS01:6666/ws/ColtService.novitas.webSvcProvider:assetMgmt/ColtService_novitas_webSvcProvider_assetMgmt_Port?wsdl";

    private static final String KEY3 = "onboarding.deployment.id";
    private static final String VALUE3 = "net.colt.novitas:onboarding:1.0";
    
    private static final String RESPONSE = "{\"environment\": \"" + ENV + "\", \"item\": \"" + KEY + "\", \"value\": \"" + VALUE + "\"}";
    private static final String RESPONSE1 = "{\"environment\": \"" + ENV + "\", \"item\": \"" + KEY + "\", \"value\": null}";
    
    private static final String RESPONSE2 = "[{\"environment\": \"" + ENV + "\", \"item\": \"" + KEY + "\", \"value\": \"" + VALUE + "\"},"
                                            + "{\"environment\": \"" + ENV + "\", \"item\": \"" + KEY2 + "\", \"value\": \"" + VALUE2 + "\"},"
                                            + "{\"environment\": \"" + ENV + "\", \"item\": \"" + KEY3 + "\", \"value\": \"" + VALUE3 + "\"}]";
    
    private static final String RESPONSE1_2 = "[{\"environment\": \"" + ENV + "\", \"item\": \"" + KEY + "\", \"value\": \"" + VALUE + "\"},"
                                            + "{\"environment\": \"" + ENV + "\", \"item\": \"" + KEY2 + "\", \"value\": null},"
                                            + "{\"environment\": \"" + ENV + "\", \"item\": \"" + KEY3 + "\", \"value\": \"" + VALUE3 + "\"}]";
    
    private String resourceApiPortUrl = "http://127.0.0.1:8080/config-api/api/configuration";
    
    /*@Tested
    private ResourceAPIClient client;

    @Mocked
    private HttpClient httpClient;
    
    @Mocked
    private GetMethod method;
    
    private List<String> keys;
    
    @Before
    public void setUp() throws Exception {
        keys = new ArrayList<String>();
        keys.add(KEY);
        keys.add(KEY2);
        keys.add(KEY3);
    }

    @Test
    public void testGetPropertySuccess() throws IOException {
        //ResourceAPIClient client = new ResourceAPIClient();
        
        new Expectations() {{
            httpClient.executeMethod((HttpMethod) any); result = 200;
            method.getResponseBodyAsStream(); result = new ByteArrayInputStream(RESPONSE.getBytes("UTF8"));
        }};
        Property property = client.getProperty(ENV, KEY);
        assertNotNull(property);
        
        assertEquals(ENV, property.getEnvironment());
        assertEquals(KEY, property.getItem());
        assertEquals(VALUE, property.getValue());
        
        verifyUrl(false);
    }
    
    @Test
    public void testGetPropertyUnauthorised() throws IOException {
        //ResourceAPIClient client = new ResourceAPIClient();
        
        new Expectations() {{
            httpClient.executeMethod((HttpMethod) any); result = 401;
            method.getResponseBodyAsStream(); result = new ByteArrayInputStream(RESPONSE.getBytes("UTF8"));
        }};
        Property property = client.getProperty(ENV, KEY);
        assertNull(property);
        
        verifyUrl(false);
        
    }
    
    
    @Test
    public void testGetPropertyNotFound() throws IOException {
        //ResourceAPIClient client = new ResourceAPIClient();
        
        new Expectations() {{
            httpClient.executeMethod((HttpMethod) any); result = 200;
            method.getResponseBodyAsStream(); result = new ByteArrayInputStream(RESPONSE1.getBytes("UTF8"));
        }};
        Property property = client.getProperty(ENV, KEY);
        assertNull(property);
        
        verifyUrl(false);
    }
    
    @Test
    public void testGetPropertyException() throws IOException {
        //ResourceAPIClient client = new ResourceAPIClient();
        
        new Expectations() {{
            httpClient.executeMethod((HttpMethod) any); result = 200;
            method.getResponseBodyAsStream(); result = new IOException();
        }};
        Property property = client.getProperty(ENV, KEY);
        assertNull(property);
        
        verifyUrl(false);
    }
    
    // ---------- multiple properties hasmap
    @Test
    public void testGetPropertiesHashMap() throws IOException {

        new Expectations() {{
            httpClient.executeMethod((HttpMethod) any); result = 200;
            method.getResponseBodyAsStream(); result = new ByteArrayInputStream(RESPONSE2.getBytes("UTF8"));
        }};
        
        Map<String, String> properties = client.getPropertiesAsMap(ENV, keys);
        assertNotNull(properties);
        
        assertEquals(3, properties.size());
        assertEquals(VALUE, properties.get(KEY));
        assertEquals(VALUE2, properties.get(KEY2));
        assertEquals(VALUE3, properties.get(KEY3));
    }
    
 // ---------- multiple properties hasmap
    @Test
    public void testGetPropertiesHashMapMissingProp() throws IOException {

        new Expectations() {{
            httpClient.executeMethod((HttpMethod) any); result = 200;
            method.getResponseBodyAsStream(); result = new ByteArrayInputStream(RESPONSE1_2.getBytes("UTF8"));
        }};
        
        Map<String, String> properties = client.getPropertiesAsMap(ENV, keys);
        assertNotNull(properties);
        
        assertEquals(3, properties.size());
        assertEquals(VALUE, properties.get(KEY));
        assertEquals(null, properties.get(KEY2));
        assertEquals(VALUE3, properties.get(KEY3));
    }
    
    //-------- Get Multiple properties
    @Test
    public void testGetPropertiesSuccess() throws IOException {
        //ResourceAPIClient client = new ResourceAPIClient();
        
        new Expectations() {{
            httpClient.executeMethod((HttpMethod) any); result = 200;
            method.getResponseBodyAsStream(); result = new ByteArrayInputStream(RESPONSE2.getBytes("UTF8"));
        }};
        
        List<Property> properties = client.getProperties(ENV, keys);
        assertNotNull(properties);
        
        assertEquals(3, properties.size());
        
        Property property = properties.get(0);
        
        assertEquals(ENV, property.getEnvironment());
        assertEquals(KEY, property.getItem());
        assertEquals(VALUE, property.getValue());
        
        verifyUrl(true);
        
    }
    
    @Test
    public void testGetPropertiesUnauthorised() throws IOException {
        //ResourceAPIClient client = new ResourceAPIClient();
        
        new Expectations() {{
            httpClient.executeMethod((HttpMethod) any); result = 401;
            method.getResponseBodyAsStream(); result = new ByteArrayInputStream("Unauthorised".getBytes("UTF8"));
        }};
        List<Property> property = client.getProperties(ENV, keys);
        assertNotNull(property);
        assertEquals(0, property.size());
        verifyUrl(true);
        
    }
    
    *//**
     * one property is not found so we expect to get back only 2 not 3
     * @throws IOException
     *//*
    @Test
    public void testGetPropertiesNotFound() throws IOException {
        //ResourceAPIClient client = new ResourceAPIClient();
        
        new Expectations() {{
            httpClient.executeMethod((HttpMethod) any); result = 200;
            method.getResponseBodyAsStream(); result = new ByteArrayInputStream(RESPONSE1_2.getBytes("UTF8"));
        }};
        List<Property> property = client.getProperties(ENV, keys);
        assertNotNull(property);
        
        assertEquals(2, property.size());
        verifyUrl(true);
        
    }
    
    @Test
    public void testGetPropertiesException() throws IOException {
        //ResourceAPIClient client = new ResourceAPIClient();
        
        new Expectations() {{
            httpClient.executeMethod((HttpMethod) any); result = 200;
            method.getResponseBodyAsStream(); result = new IOException();
        }};
        
        List<Property> property = client.getProperties(ENV, keys);
        assertNotNull(property);
        assertEquals(0, property.size());
        verifyUrl(true);
        
    }

    
    //-------- property value
    @Test
    public void testGetPropertyValueSuccess() throws IOException {
        //ResourceAPIClient client = new ResourceAPIClient();
        
        new Expectations() {{
            httpClient.executeMethod((HttpMethod) any); result = 200;
            method.getResponseBodyAsStream(); result = new ByteArrayInputStream(RESPONSE.getBytes("UTF8"));
        }};
        String value = client.getPropertyValue(ENV, KEY);
        assertNotNull(value);
        assertEquals(VALUE, value);
        verifyUrl(false);
        
    }
    
    @Test
    public void testGetPropertyValueSuccessDefault() throws IOException {
        //ResourceAPIClient client = new ResourceAPIClient();
        
        new Expectations() {{
            httpClient.executeMethod((HttpMethod) any); result = 200;
            method.getResponseBodyAsStream(); result = new ByteArrayInputStream(RESPONSE.getBytes("UTF8"));
        }};
        String value = client.getPropertyValue(ENV, KEY, "test");
        assertNotNull(value);
        assertEquals(VALUE, value);
        verifyUrl(false);
        
    }
    
    @Test
    public void testGetPropertyValueUnauthorised() throws IOException {
        //ResourceAPIClient client = new ResourceAPIClient();
        
        new Expectations() {{
            httpClient.executeMethod((HttpMethod) any); result = 401;
            method.getResponseBodyAsStream(); result = new ByteArrayInputStream(RESPONSE.getBytes("UTF8"));
        }};
        String value = client.getPropertyValue(ENV, KEY);
        assertNull(value);
        verifyUrl(false);
        
    }
    
    
    @Test
    public void testGetPropertyValueNotFound() throws IOException {
        //ResourceAPIClient client = new ResourceAPIClient();
        
        new Expectations() {{
            httpClient.executeMethod((HttpMethod) any); result = 200;
            method.getResponseBodyAsStream(); result = new ByteArrayInputStream(RESPONSE1.getBytes("UTF8"));
        }};
        String value = client.getPropertyValue(ENV, KEY);
        assertNull(value);
        verifyUrl(false);
        
    }
    
    @Test
    public void testGetPropertyValueNotFoundDefault() throws IOException {
        //ResourceAPIClient client = new ResourceAPIClient();
        
        new Expectations() {{
            httpClient.executeMethod((HttpMethod) any); result = 200;
            method.getResponseBodyAsStream(); result = new ByteArrayInputStream(RESPONSE1.getBytes("UTF8"));
        }};
        String value = client.getPropertyValue(ENV, KEY, "test");
        assertNotNull(value);
        assertEquals("test", value);
        verifyUrl(false);
        
    }
    
    @Test
    public void testGetPropertyValueException() throws IOException {
        //ResourceAPIClient client = new ResourceAPIClient();
        
        new Expectations() {{
            httpClient.executeMethod((HttpMethod) any); result = 200;
            method.getResponseBodyAsStream(); result = new IOException();
        }};
        String value = client.getPropertyValue(ENV, KEY);
        assertNull(value);
        verifyUrl(false);
        
    }
    
    private void verifyUrl(final boolean multiple) {
        new Verifications() {{
            String url;
            new GetMethod(url = withCapture());
            
            assertNotNull(url);
            
            if(multiple) {
                
                assertEquals(resourceApiPortUrl + "?environment=" + ENV + "&item=" + KEY + "&item=" + KEY2 + "&item=" + KEY3, url);
                
            } else {
                assertEquals(resourceApiPortUrl + "?environment=" + ENV + "&item=" + KEY, url);
            }
        }};
    }*/
}
