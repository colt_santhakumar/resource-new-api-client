package com.colt.novitas.client;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;

import com.colt.novitas.test.category.IntegrationTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Category(IntegrationTest.class)
public class ResourceAPIClientIntegrationTest {

    private static final String ENV = "DEV";
    private static final String KEY = "callback.api.rest.url";
    private static final String VALUE = "http://amsdcp13:8330/callback-api/rest/";
    
    private static final String KEY2 = "siebel.assets.url";
    private static final String VALUE2 = "http://WBMEAIIS01:6666/ws/ColtService.novitas.webSvcProvider:assetMgmt/ColtService_novitas_webSvcProvider_assetMgmt_Port?wsdl";

    private static final String KEY3 = "onboarding.deployment.id";
    private static final String VALUE3 = "net.colt.novitas:onboarding:1.0";
    
    //private ResourceAPIClient client = new ResourceAPIClient(TestDataService.getInstance().getTestProperty("configuration.service.reload.url"));
    
    private List<String> keys;
    
    /*@BeforeClass
    public static void setupTestData() {
        // update the database
        TestDataService service = TestDataService.getInstance();
        service.createTestData();
        updateIPAddress();
        // reload the service
        reloadConfigurations();
        
    }
    
    @Before
    public void setUp() throws Exception {
        keys = new ArrayList<String>();
        keys.add(KEY);
        keys.add(KEY2);
        keys.add(KEY3);
    }

    @Test
    public void testGetPropertySuccess() throws IOException {

        Property property = client.getProperty(ENV, KEY);
        assertNotNull(property);
        
        assertEquals(ENV, property.getEnvironment());
        assertEquals(KEY, property.getItem());
        assertEquals(VALUE, property.getValue());
        
    }
    
    @Ignore
    @Test
    public void testGetPropertyUnauthorised() throws IOException {
        ResourceAPIClient client = new ResourceAPIClient("http://localhost:8080/config-api/api/configuration");
        Property property = client.getProperty(ENV, KEY);
        assertNull(property);
        
    }
    
    
    @Test
    public void testGetPropertyNotFound() throws IOException {
    
        Property property = client.getProperty(ENV, KEY + "1");
        assertNull(property);
        
    }
    
    @Test
    public void testGetPropertyException() throws IOException {
        ResourceAPIClient client = new ResourceAPIClient("http://127.0.0.1:8089/config-api/api/configuration");
        Property property = client.getProperty(ENV, KEY);
        assertNull(property);
        
    }
    
    // ---------- multiple properties hasmap
    @Test
    public void testGetPropertiesHashMap() throws IOException {

        Map<String, String> properties = client.getPropertiesAsMap(ENV, keys);
        assertNotNull(properties);
        
        assertEquals(3, properties.size());
        assertEquals(VALUE, properties.get(KEY));
        assertEquals(VALUE2, properties.get(KEY2));
        assertEquals(VALUE3, properties.get(KEY3));
    }
    
 // ---------- multiple properties hasmap
    @Test
    public void testGetPropertiesHashMapMissingProp() throws IOException {
        
        keys.set(1, keys.get(1) + ".1");
        Map<String, String> properties = client.getPropertiesAsMap(ENV, keys);
        assertNotNull(properties);
        
        assertEquals(3, properties.size());
        assertEquals(VALUE, properties.get(KEY));
        assertEquals(null, properties.get(KEY2));
        assertEquals(VALUE3, properties.get(KEY3));
    }
    
    //-------- Get Multiple properties
    @Test
    public void testGetPropertiesSuccess() throws IOException {

        List<Property> properties = client.getProperties(ENV, keys);
        assertNotNull(properties);
        
        assertEquals(3, properties.size());
        
        Property property = properties.get(0);
        
        assertEquals(ENV, property.getEnvironment());
        assertEquals(KEY, property.getItem());
        assertEquals(VALUE, property.getValue());
        
    }
        
    *//**
     * one property is not found so we expect to get back only 2 not 3
     * @throws IOException
     *//*
    @Test
    public void testGetPropertiesNotFound() throws IOException {
        
        keys.set(1, keys.get(1) + ".1");
        
        List<Property> property = client.getProperties(ENV, keys);
        assertNotNull(property);
        
        assertEquals(2, property.size());
        
    }
    
    @Test
    public void testGetPropertiesException() throws IOException {
        ResourceAPIClient client = new ResourceAPIClient("http://localhost:8089/config-api/api/configuration");
        List<Property> property = client.getProperties(ENV, keys);
        assertNotNull(property);
        assertEquals(0, property.size());
        
    }

    
    //-------- property value
    @Test
    public void testGetPropertyValueSuccess() throws IOException {

        String value = client.getPropertyValue(ENV, KEY);
        assertNotNull(value);
        assertEquals(VALUE, value);
        
    }
    
    @Test
    public void testGetPropertyValueSuccessDefault() throws IOException {

        String value = client.getPropertyValue(ENV, KEY, "test");
        assertNotNull(value);
        assertEquals(VALUE, value);
        
    }
    
    @Test
    public void testGetPropertyValueNotFound() throws IOException {

        String value = client.getPropertyValue(ENV, KEY+ "1");
        assertNull(value);
        
    }
    
    @Test
    public void testGetPropertyValueNotFoundDefault() throws IOException {

        String value = client.getPropertyValue(ENV, KEY+ "1", "test");
        assertNotNull(value);
        assertEquals("test", value);
        
    }
    
    // executed last, 
    @Test
    public void zTestUnauthorised() throws IOException {
        TestDataService.getInstance().runSQLUpdateQuery("DELETE FROM `CLIENTS`");
        reloadConfigurations();
        
        // get property
        Property property = client.getProperty(ENV, KEY);
        assertNull(property);
        
        // get property value
        String value = client.getPropertyValue(ENV, KEY);
        assertNull(value);
        
        // get properties
        List<Property> properties = client.getProperties(ENV, keys);
        assertNotNull(properties);
        assertEquals(0, properties.size());
        
    }

    private static void reloadConfigurations() {
        try {   
            HttpClient client = new HttpClient();
            GetMethod get = new GetMethod(TestDataService.getInstance().getTestProperty("configuration.service.reload.url") + "/reload");
            // execute method and handle any error responses.
            client.executeMethod(get);
            // Process the data from the input stream.
            get.releaseConnection();

        } catch(IOException e) {
            fail("Failed to reload configurations");
        }
    }
    
    private static void updateIPAddress() {
        
        
        Enumeration<NetworkInterface> e = null;
        try {
            e = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException e1) {
            
            e1.printStackTrace();
        }

        if(e != null) {
            
            while(e.hasMoreElements()) {
                NetworkInterface n = (NetworkInterface) e.nextElement();
                Enumeration<InetAddress> ee = n.getInetAddresses();

                while (ee.hasMoreElements()) {
                    InetAddress i = (InetAddress) ee.nextElement();
                    String ipAddress = i.getHostAddress();
                    
                    if(ipAddress.contains(".")) {
                        System.out.println(ipAddress);
                        TestDataService.getInstance().runSQLUpdateQuery(
                                "INSERT INTO `CLIENTS` (`ip_address`,`client_name`) VALUES ('" + ipAddress + "','Test Server')");
                    }
                }
            }
        }

    }*/

}
