/**
 * 
 */
package com.colt.novitas.resource.service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.colt.novitas.resource.client.ResourceAPIClient;

/**
 * Spring friendly service wrapper of the resource api client
 * @author omerio
 *
 */
public class ConfigurationServiceImpl implements ConfigurationService {

    private String environment;

    private ResourceAPIClient client;

    public ConfigurationServiceImpl(String configServiceUrl, String environment) {
        super();

        this.environment = environment;

        client = new ResourceAPIClient(configServiceUrl);
    } 

    @Override
    public Map<String, String> getProperties(String... keys) {
        return getProperties(Arrays.asList(keys));
    }

    @Override
    public Map<String, String> getProperties(List<String> keys) {
        return client.getPropertiesAsMap(environment, keys);
    }

    @Override
    public String getProperty(String key) {
        return client.getPropertyValue(environment, key);
    }

    @Override
    public String getUrlAndPath(String base, String path) {
        Map<String, String> props = this.getProperties(base, path);

        return props.get(base) + props.get(path);  
    }

	public String getEnvironment() {
		return environment;
	}

	
	public ResourceAPIClient getClient() {
		return client;
	}

	    

}
