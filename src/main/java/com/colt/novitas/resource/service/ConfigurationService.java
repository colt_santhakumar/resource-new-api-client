/**
 * 
 */
package com.colt.novitas.resource.service;

import java.util.List;
import java.util.Map;

/**
 * Spring friendly service wrapper of the resource api client
 * @author omerio
 *
 */
public interface ConfigurationService {

    Map<String, String> getProperties(String... keys);

    Map<String, String> getProperties(List<String> keys);

    String getProperty(String key);

    String getUrlAndPath(String base, String path);

}
