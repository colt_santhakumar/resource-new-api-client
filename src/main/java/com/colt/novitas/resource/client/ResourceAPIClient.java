package com.colt.novitas.resource.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.colt.novitas.resource.response.Property;

/**
 * @author omerio
 *
 */
@SuppressWarnings("unchecked")
public class ResourceAPIClient {

    private static final Logger LOG = Logger.getLogger(ResourceAPIClient.class);

    private String resourceApiPortUrl = "";
    
    private RestTemplate rest = new RestTemplate();

    public ResourceAPIClient() {
        super();
    }

    public ResourceAPIClient(String resourceApiPortUrl) {
        super();
        this.resourceApiPortUrl = resourceApiPortUrl;
    }
    
    /**
     * Return multiple properties as a map keyed by item
     * @param environment
     * @param keys
     * @return
     */
    public Map<String, String> getPropertiesAsMap(final String environment, final List<String> keys) {
        Map<String, String> map = new HashMap<String, String>();
        
        List<Property> properties = getProperties(environment, keys);
        
        for(Property property: properties) {
            map.put(property.getItem(), property.getValue());
        }
        
        // put empty keys on the map
        for(String key: keys) {
            if(!map.containsKey(key)) {
                map.put(key, null);
            }
        }
        
        return map;
    }

    /**
     * Get multiple properties for the provided keys
     * @param environment
     * @param keys
     * @return a list of properties, a key with a missing value will not be in the list 
     */
    public List<Property> getProperties(final String environment, final List<String> keys) {

        if(keys == null || environment == null) {
            throw new IllegalArgumentException("both environment and key are required");
        }

        final List<Property> properties = new ArrayList<Property>();

        if(!keys.isEmpty()) {

            if(keys.size() == 1) {
            	 String url = resourceApiPortUrl +"?environment=" + environment + "&item=" + keys.get(0);
                 Property property = (Property)this.getProperties( url,Property.class);
                 if(property != null) {
                    properties.add(property);
                 }

            } else {

                // construct the URL
                StringBuilder url = new StringBuilder(resourceApiPortUrl).append("?environment=").append(environment); 
                for(String key: keys) {
                    url.append("&item=").append(key);
                }
                Object resp = this.getProperties( url.toString(),Property[].class);
                if(resp != null) {
                	return Arrays.asList((Property[]) resp);
                }
            }
        }

        return properties;
    }

    /**
     * Get a property object for the provided environment and key
     * @param environment
     * @param key
     * @return Property if found, otherwise null
     */
    public Property getProperty(final String environment, final String key) {

        if(key == null || environment == null) {
            throw new IllegalArgumentException("both environment and key are required");
        }

        String url = resourceApiPortUrl +"?environment=" + environment + "&item=" + key;

        return  (Property)this.getProperties( url,Property.class);
      
    }

    /**
     * A generic connect method, instead of copy-paste the code in multiple methods
     * @param url
     * @param callback
     */
    /* protected void connect(String url, Callback callback) {
        
    	CloseableHttpClient httpclient = HttpClients.createDefault();
		CloseableHttpResponse response = null;
        try {
        	HttpGet httpGet = new HttpGet(url);
   		    httpGet.addHeader("Content-Type", "application/json;charset=UTF-8");

   		    if(MDC.get("novitasApiId")!=null){
   		    	httpGet.addHeader("x-NovitasApiId", (String)MDC.get("novitasApiId"));
			}
   			
   		    response = httpclient.execute(httpGet);
   		    HttpEntity entity = response.getEntity();
   		    String responseBody = EntityUtils.toString(entity, Charset.forName("UTF-8"));
   		    int responseCode = response.getStatusLine().getStatusCode() ;
            if (responseCode == 200) {
                callback.success(responseBody, responseCode);
            } else {
                LOG.error("Error response returned, code: " + responseCode + ", reponse: " + response);
                callback.failure(responseBody, responseCode);
            }
            EntityUtils.consume(entity);
        } catch (IOException e) {
            LOG.error("API call failed", e);
        } finally {
        	if(null != response){
				try {
					response.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
        }
        
    	
    }*/


    /**
     * Get the value for the key and environment
     * @param environment
     * @param key
     * @return the value for the provided key or null if not found
     */
    public String getPropertyValue(String environment, String key) {
        return this.getPropertyValue(environment, key, null);
    }

    /**
     * Get the value for the key and enviroment
     * @param environment
     * @param key
     * @param defaultValue
     * @return the actual value or defaultValue if the actual value is null
     */
    public String getPropertyValue(String environment, String key, String defaultValue) {

        String value = defaultValue;

        Property property = this.getProperty(environment, key);

        if(property != null) {
            value = property.getValue();
        }

        return value;

    }

    /**
     * Populates the provided property from the jsonObject
     * @param property
     * @param jsonObj
     * @return true if the jsonObj has values
     */
 /*   private boolean populatePropertyFromJsonObject(Property property, JSONObject jsonObj) {
        boolean populated = true;

        if (!jsonObj.get("environment").toString().equals("null")) {
            property.setEnvironment(jsonObj.get("environment").toString());
        }

        if (!jsonObj.get("item").toString().equals("null")) {
            property.setItem(jsonObj.get("item").toString());
        }

        if (!jsonObj.get("value").toString().equals("null")) {
            property.setValue(jsonObj.get("value").toString());

        } else {
            populated = false;
        }

        return populated;
    }*/


   /* public interface Callback {

        void success(String response, int code);

        void failure(String response, int code);
    }*/


	public String getResourceApiPortUrl() {
		return resourceApiPortUrl;
	}

   private <T> T getProperties(String url, Class<T> type) {
    	 
	ResponseEntity<T> response = null;
	try {
	response = rest.exchange(url, HttpMethod.GET,
				new HttpEntity(new HttpHeaders() {
					{   
						set("Content-Type", "application/json;charset=UTF-8");
						if(MDC.get("novitasApiId")!=null){
				   		    	set("x-NovitasApiId", (String)MDC.get("novitasApiId"));
						}
					}
				}),type);
     
		return  response.getBody();
	}catch (Exception e) {
		LOG.error("API call is failed",e);
		return null;
	}	
   }
  
}
